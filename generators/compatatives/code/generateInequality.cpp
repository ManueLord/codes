#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

int main(int argc, char **argv){
    srand((int)time(0));
    int max, num1, num2;
    for (int i = 1; i < argc; i++) {
        string data = argv[i];
        max = stoi( data );
    }   
    num1 = rand() % max + 1;
    num2 = rand() % max + 1;
    string res = " ";
    if (num1 > num2){
        res = "mayor_que";
    }
    else if (num1 < num2){
        res = "menor_que";
    }
    else{
        res = "igual";
    }
    cout << num1 << " " << num2 << " " << res << endl;
    return 0;
}
